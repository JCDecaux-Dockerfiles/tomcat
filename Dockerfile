FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

RUN yum install supervisor tomcat -y \
  && yum clean all

COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/tomcat"]

EXPOSE 8080

CMD ["/usr/bin/supervisord"]
